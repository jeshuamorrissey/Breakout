﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    [Tooltip("The intial speed of the ball. When it is created, it will be send " +
             "at this speed in both axes towards the top right of the screen.")]
    public float InitialSpeed = 1f;

    // The rigid body for this ball.
    Rigidbody2D _body;

    // True iff the ball has been "fired" from the paddle.
    bool _isBallInPlay;

    void Start()
    {
        _body = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && !_isBallInPlay)
        {
            _isBallInPlay = true;
            _body.isKinematic = false;
            _body.velocity = new Vector2(InitialSpeed, InitialSpeed);
        }
    }
}
