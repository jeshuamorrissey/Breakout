﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Tooltip("The global GameManager instance. This class is a singleton.")]
    public static GameManager Instance = null;

    [Tooltip("The text which will be updated with the number of bricks still left.")]
    public Text BricksLeftText;

    [Tooltip("The text which will be updated with the number of lives left.")]
    public Text LivesLeftText;

    [Tooltip("The final game over text, which will be updated based on whether the player loses or not.")]
    public Text GameOverText;

    [Tooltip("The delay (in seconds) to wait before resetting the game.")]
    public float ResetDelay = 4f;

    [Tooltip("The timescale to set the game to on Game Over.")]
    public float GameOverTimeScale = 0.25f;

    [Tooltip("The number of lives the player has.")]
    public int Lives = 3;

    [Tooltip("The prefab which can be used to create the bricks.")]
    public GameObject BricksPrefab;

    [Tooltip("The prefab which can be used to create the paddle + ball.")]
    public GameObject PaddlePrefab;

    // The created paddle (so we can reset the position when they lose a life).
    GameObject _paddle;

    // The number of bricks remaining. Will be initially set to the number of
    // gameObjects with the Brick component type.
    int _bricks = 0;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        GameOverText.text = "";
        LivesLeftText.text = "Lives Left: " + Lives;
        _paddle = Instantiate(PaddlePrefab, PaddlePrefab.transform.position, Quaternion.identity);
        Instantiate(BricksPrefab, BricksPrefab.transform.position, Quaternion.identity);

        // Count the number of bricks.
        _bricks = FindObjectsOfType<Brick>().Length;
    }

    // Inform the game manager that a brick has been destroyed.
    public void DestroyBrick()
    {
        _bricks--;
        BricksLeftText.text = "Bricks: " + _bricks;

        if (_bricks <= 0)
        {
            GameOver("You Win!");
        }
    }

    // Inform the game manager that a life has just been lost.
    public void LoseLife()
    {
        Lives--;
        LivesLeftText.text = "Lives Left: " + Lives;

        if (Lives <= 0)
        {
            GameOver("Game Over!");
        }
        else
        {
            // They are still alive, so reset the paddle + ball.
            Destroy(_paddle);
            _paddle = Instantiate(PaddlePrefab, PaddlePrefab.transform.position, Quaternion.identity);
        }
    }

    // Display a Game Over message, set the time scale to 0.25 and wait for a reset.
    private void GameOver(string message)
    {
        GameOverText.text = message;
        Time.timeScale = GameOverTimeScale;
        Invoke("ResetGame", ResetDelay * GameOverTimeScale);
    }

    // Reload the game and reset the time scale.
    private void ResetGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
