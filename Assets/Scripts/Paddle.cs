﻿using UnityEngine;

public class Paddle : MonoBehaviour
{
    [Tooltip("The horizontal speed of the paddle. Higher numbers are faster.")]
    public float Speed = 7.5f;

    // The rigid body for this paddle.
    Rigidbody2D _body;
    
    // Use this for initialization
    void Start()
    {
        _body = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _body.velocity = new Vector2(
            Input.GetAxis("Horizontal") * Speed,
            _body.velocity.y);
    }
}
