﻿using UnityEngine;

public class Brick : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Only count collisions with a "ball". If a ball hits us, we should die.
        if (collision.gameObject.GetComponent<Ball>() != null)
        {
            GameManager.Instance.DestroyBrick();
            Destroy(gameObject);
        }
    }
}
