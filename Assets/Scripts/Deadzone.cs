﻿using UnityEngine;

public class Deadzone : MonoBehaviour
{
    private void OnTriggerEnter2D()
    {
        GameManager.Instance.LoseLife();
    }
}
